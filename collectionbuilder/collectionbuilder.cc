#include<iostream>
#include<fstream>
using namespace std;
int main(int argc, char * argv[]){
	cout<<"Collection builder version 0.1."<<endl;
	string filepath = "";
	if(argc>2){
		cout<<"Enter location to save file to: ";
		getline(cin, filepath);
	} else if(argc == 2){
		filepath = argv[1];
	}
	else{
		cerr<<"Too many arguments. Taking manual input for file name."<<endl;
		cout<<"Enter location to save file to: ";
		getline(cin, filepath);
	}
	return 0;
}
