#include"listhelper/listhelper.cc"
#include <iostream>
int main(){
	singlelinked<int> test1;
	test1.push_back(1);//0
	test1.push_back(2);
	test1.push_back(3);
	test1.push_back(4);
	test1.push_back(5);
	test1.push_back(6);
	test1.push_back(7);
	test1.push_back(8);
	test1.push_back(9);
	test1.push_back(10);//9
	test1.push_back(11);
	test1.push_back(12);
	test1.push_back(13);
	test1.push_back(14);
	test1.push_back(15);
	test1.push_back(16);
	test1.push_back(17);
	test1.push_back(18);
	test1.push_back(19);
	test1.push_back(20);//19
	std::cout<<"Element 4 is: "<<test1[4]<<std::endl;
	test1.clearone(4);
	std::cout<<"Element 4 was removed, should now be +1: "<<test1[4]<<std::endl;
	int size = test1.size();
	std::cout<<"Should be 19: "<<size<<std::endl;
	std::cout<<"Should be 1: "<<test1[0]<<std::endl;
	std::cout<<"Should be 1: "<<(*test1.getelement(0))<<std::endl;
	test1.clearall();
	std::cout<<"Should be 0: "<<test1.size()<<std::endl;
	return 0;
}
