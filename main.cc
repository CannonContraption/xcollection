#include "listhelper/listhelper.cc"
#include "XCollection/xcollection.cc"
#include "fileparser/parser.cc"
#include "fileparser/filesave.cc"

int main(){
	//Commands
	string viewall = "view";
	string viewalbum = "view album";
	string viewartist = "view artist";
	string addartist = "add artist";
	string addalbum = "add album";
	string addtrack = "add track";
	string openfile = "open";
	string saverecords = "save";
	string editartist = "edit artist";
	string editalbum = "edit album";
	string edittrack = "edit track";
	string deleteartist = "delete artist";
	string deletealbum = "delete album";
	string deletetrack = "delete track";
	
	//command storage
	string artist;
	string albumname;
	string albumgenre;
	int albumyear;
	string temp;
	string trackname;
	int hours;
	int minutes;
	int seconds;
	string fname;
	
	//the collection itself
	xcollection fromfile;
	
	//we start taking questions
	cout<<"Command entry. \"help\" for more information. Ctrl-D or \"exit\" quits."<<endl;
	string command;
	while(!cin.fail()){
		//our prompt should be familiar to ed users
		cout<<"? ";
		getline(cin,command);
		if(cin.fail()) command = "exit";
		if (command == addartist){
			getline(cin,artist);
			fromfile.addartist(artist);
		}
		else if (command == openfile){
			getline(cin, fname);
			int parseresults = parsefile(&fromfile, fname);
			if(parseresults!=0) cerr<<"Error loading \""<<fname<<'"'<<endl;
			if(parseresults==1) cerr<<"The file may be corrupt."<<endl;
			if(parseresults==2) cerr<<"Make sure the file exists and you have read permissions."<<endl;
		}
		else if (command == saverecords){
			getline(cin,fname);
			if(savefile(&fromfile, fname)!=0) cerr<<"Error saving \""<<fname<<'"'<<endl;
		}
		else if (command == addalbum){
			cout<<"Enter album information as follows: artist-name <enter> album-name <enter> album-genre <enter> album-year <enter>."<<endl;
			getline(cin, artist);
			getline(cin, albumname);
			getline(cin, albumgenre);
			getline(cin,temp);
			albumyear = stoi(temp);
			fromfile.addalbum(artist, albumname, albumgenre, albumyear);
		}
		else if (command == addtrack){
			cout<<"Enter track information as follows: artist name <enter> album name <enter> track name <enter> hours <enter> minutes <enter> seconds <enter>. Use spaces or line breaks to divide hours minutes and seconds."<<endl;
			getline(cin, artist);
			getline(cin, albumname);
			getline(cin, trackname);
			getline(cin, temp);
			hours = stoi(temp);
			getline(cin, temp);
			minutes = stoi(temp);
			getline(cin, temp);
			seconds = stoi(temp);
			fromfile.addtrack(artist, albumname, trackname, hours, minutes, seconds);
		}
		else if (command == editartist){
			string oldname;
			string newname;
			cout<<"Enter current artist name:"<<endl;
			getline(cin, oldname);
			cout<<"Enter new artist name:"<<endl;
			getline(cin, newname);
			if(fromfile.editartist(oldname, newname)) cout<<"Success."<<endl;
			else cout<<"Unable to update artist. Check to make sure that artist exists. Type "<<viewartist<<" to see a list of available artists."<<endl;
		}
		else if (command == editalbum){
			string artist;
			string oldname;
			string newname;
			string newgenre;
			string newyeartemp;
			int newyear;
			cout<<"Enter artist name:"<<endl;
			getline(cin, artist);
			cout<<"Enter current album name:"<<endl;
			getline(cin, oldname);
			cout<<"Enter new album name:"<<endl;
			getline(cin, newname);
			cout<<"Enter new album genre:"<<endl;
			getline(cin, newgenre);
			cout<<"Enter new album year:"<<endl;
			getline(cin, newyeartemp);
			newyear = stoi(newyeartemp);
			if(fromfile.editalbum(artist, oldname, newname, newgenre, newyear)) cout<<"Success."<<endl;
			else cout<<"Unable to update album. Check to make sure the artist and album exist. Type "<<viewalbum<<" to see a list of available artists and their albums."<<endl;
		}
		else if (command == deleteartist){
			cout<<"Enter artist name:"<<endl;
			getline(cin, artist);
			if(fromfile.deleteartist(artist)) cout<<"good"<<endl;
			else cout<<"Failed."<<endl;
		}
		else if (command == deletealbum){
			cout<<"Enter Artist Name:"<<endl;
			getline(cin, artist);
			cout<<"Enter album name:"<<endl;
			getline(cin, albumname);
			if(fromfile.deletealbum(artist, albumname))cout<<"good"<<endl;
			else cout<<"Failed."<<endl;
		}
		else if (command == deletetrack){
			cout<<"Enter Artist Name:"<<endl;
			getline(cin, artist);
			cout<<"Enter Album Name:"<<endl;
			getline(cin, albumname);
			cout<<"Enter Track Name:"<<endl;
			getline(cin, trackname);
			if(fromfile.deletealbum(artist, albumname)) cout<<"Good"<<endl;
			else cout<<"failed"<<endl;
		}
		else if (command == viewall)
			fromfile.showartists(true, true);
		else if (command == viewalbum)
			fromfile.showartists(true, false);
		else if(command == viewartist)
			fromfile.showartists(false, false);
		else if(command == "help"){
			cout<<"Commands:"<<endl<<viewall<<endl<<viewalbum<<endl<<viewartist<<endl<<addartist<<endl<<addalbum<<endl<<addtrack<<endl<<openfile<<endl<<saverecords<<endl<<"help"<<endl<<editartist<<endl<<editalbum<<endl<<edittrack<<endl<<deleteartist<<endl<<deletealbum<<endl<<deletetrack<<endl;
		}
		else if(command == "exit"){
			cout<<"exit"<<endl;
			return 0;
		}
		else {
			cout<<"Command "<<command<<" invalid. Type \"help\" for more information."<<endl;
		}
	}
	return 1;
}
