#include<gtkmm.h>
#include "listhelper/listhelper.cc"
#include "XCollection/xcollection.cc"
#include "fileparser/parser.cc"
#include "fileparser/filesave.cc"

xcollection currentcollection;

Gtk::ApplicationWindow * toplevel;

//The artist GTK+ Strutures
Glib::RefPtr<Gtk::TreeSelection> artist_selection;
Glib::RefPtr<Gtk::ListStore> artist_listing;
Gtk::TreeModelColumn<string> artist_name;

//Album GTK+ structures
Gtk::TreeView * album_view;
Glib::RefPtr<Gtk::TreeSelection> album_selection;
Glib::RefPtr<Gtk::ListStore> album_listing;
Gtk::TreeModelColumn<int> album_year;
Gtk::TreeModelColumn<string> album_name;
Gtk::TreeModelColumn<string> album_genre;

//Track GTK+ structures
Glib::RefPtr<Gtk::TreeSelection> track_selection;
Glib::RefPtr<Gtk::ListStore> track_listing;
Gtk::TreeModelColumn<string> track_name;
Gtk::TreeModelColumn<string> track_time;

Gtk::Button * add_track_button;
Gtk::Button * add_album_button;
Gtk::Button * add_artist_button;
Gtk::Button * edit_track_button;
Gtk::Button * edit_album_button;
Gtk::Button * edit_artist_button;
Gtk::Button * delete_track_button;
Gtk::Button * delete_album_button;
Gtk::Button * delete_artist_button;

void open_collection_file(){
	artist_listing->clear();
	album_listing->clear();
	track_listing->clear();
	Gtk::FileChooserDialog whichfile(*toplevel, "Choose a collection ini file", Gtk::FileChooserAction::FILE_CHOOSER_ACTION_OPEN);
	whichfile.add_button("Open", 3);
	whichfile.add_button("Cancel",1);
	int response = whichfile.run();
	string filename_to_open;
	if(response == 3){
		filename_to_open = whichfile.get_filename();
		int parseresults = parsefile(&currentcollection, filename_to_open);
		if(parseresults == 0){
			//currentcollection.showartists(true, true);
			for(int i = 0; i<currentcollection.collection.size(); i++){
				Gtk::TreeModel::iterator current_iterator = artist_listing->append();
				Gtk::TreeModel::Row current_row = *current_iterator;
				current_row[artist_name] = currentcollection.collection.getelement(i)->name;
			}
		} else {
			Gtk::MessageDialog ferrordialog(*toplevel, "Error loading file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK);
			if(parseresults == 1)
				ferrordialog.set_secondary_text("The file you selected may be corrupt.");
			if(parseresults == 2)
				ferrordialog.set_secondary_text("Make sure the file exists and you have read access.");
			else
				ferrordialog.set_secondary_text("Unknown error.");
			ferrordialog.run();
		}
	}
}

void save_collection_file(){
	Gtk::FileChooserDialog whichfile(*toplevel, "Choose a collection ini file", Gtk::FileChooserAction::FILE_CHOOSER_ACTION_SAVE);
	whichfile.add_button("_Save", 3);
	whichfile.add_button("Cancel",1);
	int response = whichfile.run();
	string filename_to_open;
	if(response == 3){
		filename_to_open = whichfile.get_filename();
		if(savefile(&currentcollection, filename_to_open) != 0){
			Gtk::MessageDialog ferrordialog(*toplevel, "Error saving file.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK);
			ferrordialog.set_secondary_text("Please ensure you have write permissions for that directory.");
			ferrordialog.run();
		}
	}
}

void select_artist(){
	Gtk::TreeModel::iterator iter = artist_selection->get_selected();
	if(iter){
		edit_artist_button->set_sensitive(true);
		add_album_button->set_sensitive(true);
		delete_artist_button->set_sensitive(true);
		Gtk::TreeModel::Row row = *iter;
		string artistName = row[artist_name];
		//cout<<"Selected "<<artistName<<endl;
		singlelinked<album> albums;
		//string aname;
		bool found = false;
		for(int i = 0; i<currentcollection.collection.size(); i++){
			//aname=row[artist_name];
			if(artistName == currentcollection.collection.getelement(i)->name){
				albums = currentcollection.collection.getelement(i)->albums;
				found = true;
				break;
			}
		}
		if(!found) return;
		album_listing->clear();
		track_listing->clear();
		string name;
		string genre;
		int year;
		for(int i = 0; i<albums.size(); i++){
			Gtk::TreeModel::iterator current_iterator = album_listing->append();
			Gtk::TreeModel::Row current_row = *current_iterator;
			current_row[album_name] = albums.getelement(i)->name;
			current_row[album_genre] = albums.getelement(i)->genre;
			current_row[album_year] = albums.getelement(i)->year;
		}
	} else{
		edit_artist_button->set_sensitive(false);
		add_album_button->set_sensitive(false);
		delete_artist_button->set_sensitive(false);
	}
}

void select_album(){
	Gtk::TreeModel::iterator iter = album_selection->get_selected();
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	if(iter){
		add_track_button->set_sensitive(true);
		edit_album_button->set_sensitive(true);
		delete_album_button->set_sensitive(true);
		Gtk::TreeModel::Row row = *iter;
		Gtk::TreeModel::Row artrow = *artiter;
		string albumName = row[album_name];
		string artistName = artrow[artist_name];
		//cout<<"Selected "<<artistName<<" / "<<albumName<<endl;
		singlelinked<track> tracks;
		singlelinked<album> albums;
		bool found = false;
		for(int i = 0; i<currentcollection.collection.size(); i++){
			if(artistName == currentcollection.collection.getelement(i)->name){
				albums = currentcollection.collection.getelement(i)->albums;
				found = true;
				break;
			}
		}
		if(!found) return;
		found = false;
		for(int i = 0; i<albums.size(); i++){
			if(albumName == albums.getelement(i)->name){
				tracks = albums.getelement(i)->tracks;
				found = true;
				break;
			}
		}
		if(!found) return;
		track_listing->clear();
		string name;
		string time;
		for(int i = 0; i<tracks.size(); i++){
			Gtk::TreeModel::iterator current_iterator = track_listing->append();
			Gtk::TreeModel::Row current_row = *current_iterator;
			time = to_string(tracks[i].hours) + ":" + to_string(tracks[i].minutes) + ":" + to_string(tracks[i].seconds);
			current_row[track_name]=tracks[i].title;
			current_row[track_time]=time;
		}
	} else{
		edit_album_button->set_sensitive(false);
		add_track_button->set_sensitive(false);
		delete_album_button->set_sensitive(false);
	}
}

void select_track(){
	Gtk::TreeModel::iterator iter = track_selection->get_selected();
	Gtk::TreeModel::iterator albiter = album_selection->get_selected();
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	if(iter && albiter && artiter){
		edit_track_button->set_sensitive(true);
		delete_track_button->set_sensitive(true);
		Gtk::TreeModel::Row row = *iter;
		Gtk::TreeModel::Row albrow = *albiter;
		Gtk::TreeModel::Row artrow = *artiter;
		string trackname = row[track_name];
		string artistname = artrow[artist_name];
		string albumname = albrow[album_name];
		//cout << "Selected " << artistname << " / " << albumname << " / " << trackname << endl;
	} else{
		edit_track_button->set_sensitive(false);
		delete_track_button->set_sensitive(false);
	}
}

void add_artist(){
	//cout<<"Add Artist clicked"<<endl;
	Gtk::Dialog artistAdder("Add Artist", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
	Gtk::Box * avbox = artistAdder.get_vbox();
	Gtk::Label aname_label("Artist Name");
	Gtk::Entry name_entry;
	avbox->pack_start(aname_label, false, false, 3);
	avbox->pack_start(name_entry, false, false, 3);
	artistAdder.add_button("Add", 0);
	artistAdder.add_button("Cancel", 1);
	avbox->show_all();
	int result = artistAdder.run();
	if(result == 0){
		string artistname=name_entry.get_text();
		currentcollection.addartist(artistname);
		Gtk::TreeModel::iterator current_iterator = artist_listing->append();
		Gtk::TreeModel::Row current_row = *current_iterator;
		current_row[artist_name]=artistname;
	}
}

void add_album(){
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	if(artiter){
		//cout<<"Add Album clicked"<<endl;
		Gtk::Dialog albumAdder("Add Album", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
		Gtk::Box * avbox = albumAdder.get_vbox();
		Gtk::Entry name_entry;
		Gtk::Label aname_label("Artist Name");
		Gtk::Label agenre_label("Genre");
		Gtk::Label ayear_label("Year");
		Gtk::Entry genre_entry;
		Gtk::Entry year_entry;
		avbox->pack_start(aname_label, false, false, 3);
		avbox->pack_start(name_entry, false, false, 3);
		avbox->pack_start(agenre_label, false, false, 3);
		avbox->pack_start(genre_entry, false, false, 3);
		avbox->pack_start(ayear_label, false, false, 3);
		avbox->pack_start(year_entry, false, false, 3);
		albumAdder.add_button("Add", 0);
		albumAdder.add_button("Cancel", 1);
		avbox->show_all();
		int response = albumAdder.run();
		if(response == 0){
			Gtk::TreeModel::Row artrow = *artiter;
			string artistname = artrow[artist_name];
			string albumname = name_entry.get_text();
			string albumgenre = genre_entry.get_text();
			string yearstring = year_entry.get_text();
			int albumyear = stoi(yearstring);
			//cout<<artistname<<" "<<albumname<<" "<<albumgenre<<" "<<albumyear<<endl;
			currentcollection.addalbum(artistname, albumname, albumgenre, albumyear);
			Gtk::TreeModel::iterator current_iterator = album_listing->append();
			Gtk::TreeModel::Row current_row = *current_iterator;
			current_row[album_name]=albumname;
			current_row[album_genre]=albumgenre;
			current_row[album_year]=albumyear;
		}
	}
	else{
		Gtk::MessageDialog warning(*toplevel, "No Artist Selection");
		warning.set_secondary_text("You need to select an artist to add an album.");
		warning.run();
	}
}

void add_track(){
	Gtk::TreeModel::iterator albiter = album_selection->get_selected();
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	if(albiter && artiter){
		//cout<<"Add Track clicked"<<endl;
		Gtk::Dialog trackAdder("Add Track", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
		Gtk::Box * avbox = trackAdder.get_vbox();
		Gtk::Entry name_entry;
		Gtk::Label name_label("Track Name");
		Gtk::Label time_label("Track Time");
		Gtk::HBox hms(false, 0);
		Gtk::SpinButton hours_entry(1, 2);
		hours_entry.set_range(0, 1000);
		hours_entry.set_increments(1, 5);
		Gtk::SpinButton minutes_entry(1, 2);
		minutes_entry.set_range(0, 59);
		minutes_entry.set_increments(1, 5);
		Gtk::SpinButton seconds_entry(1, 2);
		seconds_entry.set_range(0, 59);
		seconds_entry.set_increments(1,5);
		Gtk::Label hours_label("H");
		Gtk::Label minutes_label("M");
		Gtk::Label seconds_label("S");
		hms.pack_start(hours_label, false, false, 2);
		hms.pack_start(hours_entry, true, true, 2);
		hms.pack_start(minutes_label, false, false, 2);
		hms.pack_start(minutes_entry, true, true, 2);
		hms.pack_start(seconds_label, false, false, 2);
		hms.pack_start(seconds_entry, true, true, 2);
		avbox->pack_start(name_label, false, false, 4);
		avbox->pack_start(name_entry, false, false, 4);
		avbox->pack_start(time_label, false, false, 4);
		avbox->pack_start(hms, false, false, 4);
		trackAdder.add_button("Add", 0);
		trackAdder.add_button("Cancel", 1);
		avbox->show_all();
		int result = trackAdder.run();
		if(result == 0){
			string trackname = name_entry.get_text();
			int hours = (int)hours_entry.get_value();
			int minutes = (int)minutes_entry.get_value();
			int seconds = (int)seconds_entry.get_value();
			Gtk::TreeModel::Row albrow = *albiter;
			Gtk::TreeModel::Row artrow = *artiter;
			string artistname = artrow[artist_name];
			string albumname = albrow[album_name];
			currentcollection.addtrack(artistname, albumname, trackname, hours, minutes, seconds);
			Gtk::TreeModel::iterator current_iterator = track_listing->append();
			Gtk::TreeModel::Row current_row = *current_iterator;
			string time = to_string(hours) + ":" + to_string(minutes) + ":" + to_string(seconds);
			current_row[track_name]=trackname;
			current_row[track_time]=time;
		}
//		else{
//			cout<<"Cancelled track add."<<endl;
//		}
	}
	else{
		Gtk::MessageDialog warning(*toplevel, "No Artist/Album Selection");
		warning.set_secondary_text("You need to select and artist and album to add a track.");
		warning.run();
	}
}

void edit_artist(){
	Gtk::TreeModel::iterator current_iterator = artist_selection->get_selected();
	if(current_iterator){
		Gtk::TreeModel::Row current_row = *current_iterator;
		Gtk::Dialog artistEdit("Edit Artist", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
		Gtk::Box * avbox = artistEdit.get_vbox();
		Gtk::Label aname_label("Artist Name");
		Gtk::Entry name_entry;
		avbox->pack_start(aname_label, false, false, 3);
		avbox->pack_start(name_entry, false, false, 3);
		artistEdit.add_button("Apply", 0);
		artistEdit.add_button("Cancel", 1);
		avbox->show_all();
		int result = artistEdit.run();
		if(result == 0){
			string artistname=name_entry.get_text();
			string oldname = current_row[artist_name];
			currentcollection.editartist(oldname, artistname);
			current_row[artist_name]=artistname;
		}
	} else {
		Gtk::MessageDialog errordialog(*toplevel, "Error Editing Artist.", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK);
		errordialog.set_secondary_text("Select an artist to edit before clicking edit artist.");
		errordialog.run();
	}
}

void edit_album(){
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	Gtk::TreeModel::iterator current_iterator = album_selection->get_selected();
	if(artiter && current_iterator){
		Gtk::TreeModel::Row current_row = *current_iterator;
		//cout<<"Add Album clicked"<<endl;
		Gtk::Dialog albumAdder("Edit Album", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
		Gtk::Box * avbox = albumAdder.get_vbox();
		Gtk::Entry name_entry;
		Gtk::Label aname_label("Artist Name");
		Gtk::Label agenre_label("Genre");
		Gtk::Label ayear_label("Year");
		Gtk::Entry genre_entry;
		Gtk::Entry year_entry;
		avbox->pack_start(aname_label, false, false, 3);
		avbox->pack_start(name_entry, false, false, 3);
		avbox->pack_start(agenre_label, false, false, 3);
		avbox->pack_start(genre_entry, false, false, 3);
		avbox->pack_start(ayear_label, false, false, 3);
		avbox->pack_start(year_entry, false, false, 3);
		albumAdder.add_button("Apply", 0);
		albumAdder.add_button("Cancel", 1);
		avbox->show_all();
		int response = albumAdder.run();
		if(response == 0){
			string oldname = current_row[album_name];
			Gtk::TreeModel::Row artrow = *artiter;
			string artistname = artrow[artist_name];
			string albumname = name_entry.get_text();
			string albumgenre = genre_entry.get_text();
			string yearstring = year_entry.get_text();
			int albumyear = stoi(yearstring);
			//cout<<artistname<<" "<<albumname<<" "<<albumgenre<<" "<<albumyear<<endl;
			currentcollection.editalbum(artistname, oldname, albumname, albumgenre, albumyear);
			current_row[album_name]=albumname;
			current_row[album_genre]=albumgenre;
			current_row[album_year]=albumyear;
		}
	}
	else{
		Gtk::MessageDialog warning(*toplevel, "No Artist/Album Selection");
		warning.set_secondary_text("You need to select an album to edit it.");
		warning.run();
	}
}

void edit_track(){
	Gtk::TreeModel::iterator albiter = album_selection->get_selected();
	Gtk::TreeModel::iterator artiter = artist_selection->get_selected();
	Gtk::TreeModel::iterator current_iterator = track_selection->get_selected();
	if(albiter && artiter && current_iterator){
		Gtk::TreeModel::Row current_row = *current_iterator;
		Gtk::Dialog trackAdder("Edit Track", *toplevel, Gtk::DIALOG_USE_HEADER_BAR);
		Gtk::Box * avbox = trackAdder.get_vbox();
		Gtk::Entry name_entry;
		Gtk::Label name_label("Track Name");
		Gtk::Label time_label("Track Time");
		Gtk::HBox hms(false, 0);
		Gtk::SpinButton hours_entry(1, 2);
		hours_entry.set_range(0, 1000);
		hours_entry.set_increments(1, 5);
		Gtk::SpinButton minutes_entry(1, 2);
		minutes_entry.set_range(0, 59);
		minutes_entry.set_increments(1, 5);
		Gtk::SpinButton seconds_entry(1, 2);
		seconds_entry.set_range(0, 59);
		seconds_entry.set_increments(1,5);
		Gtk::Label hours_label("H");
		Gtk::Label minutes_label("M");
		Gtk::Label seconds_label("S");
		hms.pack_start(hours_label, false, false, 2);
		hms.pack_start(hours_entry, true, true, 2);
		hms.pack_start(minutes_label, false, false, 2);
		hms.pack_start(minutes_entry, true, true, 2);
		hms.pack_start(seconds_label, false, false, 2);
		hms.pack_start(seconds_entry, true, true, 2);
		avbox->pack_start(name_label, false, false, 4);
		avbox->pack_start(name_entry, false, false, 4);
		avbox->pack_start(time_label, false, false, 4);
		avbox->pack_start(hms, false, false, 4);
		trackAdder.add_button("Apply", 0);
		trackAdder.add_button("Cancel", 1);
		avbox->show_all();
		int result = trackAdder.run();
		if(result == 0){
			string oldname = current_row[track_name];
			string trackname = name_entry.get_text();
			int hours = (int)hours_entry.get_value();
			int minutes = (int)minutes_entry.get_value();
			int seconds = (int)seconds_entry.get_value();
			Gtk::TreeModel::Row albrow = *albiter;
			Gtk::TreeModel::Row artrow = *artiter;
			string artistname = artrow[artist_name];
			string albumname = albrow[album_name];
			currentcollection.edittrack(artistname, albumname, oldname, trackname, hours, minutes, seconds);
			string time = to_string(hours) + ":" + to_string(minutes) + ":" + to_string(seconds);
			current_row[track_name]=trackname;
			current_row[track_time]=time;
		}
//		else{
//			cout<<"Cancelled track add."<<endl;
//		}
	}
	else{
		Gtk::MessageDialog warning(*toplevel, "No Artist/Album Selection");
		warning.set_secondary_text("You need to select and artist and album to add a track.");
		warning.run();
	}
}

int main(int argc, char* argv[]){
	auto app = Gtk::Application::create(argc, argv, "io.github.canoncontraption.XCollectionGTK");
	toplevel = new Gtk::ApplicationWindow;
	toplevel->set_default_size(900,600);
	
	
	//Header bar contents. This is stuff that will show up in the titlebar
	//(header bar)
	Gtk::HeaderBar tbar;
	//We need window buttons, tell GTK this
	tbar.set_show_close_button(true);
	tbar.set_title("XCollection");
	tbar.set_subtitle("The Music Collection Manager");
	//The window needs to use this as its titlebar widget
	toplevel->set_titlebar(tbar);
	//And we need to create and connect open and save buttons.
	Gtk::Button open("Open");
	open.signal_clicked().connect(sigc::ptr_fun(&open_collection_file));
	tbar.pack_start(open);
	Gtk::Button save("Save");
	save.signal_clicked().connect(sigc::ptr_fun(&save_collection_file));
	tbar.pack_start(save);
	add_artist_button = new Gtk::Button("Add Artist");
	//tbar.pack_end(add_artist_button);
	add_artist_button->signal_clicked().connect(sigc::ptr_fun(&add_artist));
	add_album_button = new Gtk::Button("Add Album");
	//tbar.pack_end(add_album_button);
	add_album_button->signal_clicked().connect(sigc::ptr_fun(&add_album));
	add_track_button = new Gtk::Button("Add Track");
	//tbar.pack_end(add_track_button);
	add_track_button->signal_clicked().connect(sigc::ptr_fun(&add_track));
	
	//Here we handle the selection box for artists. A lot of this is
	//just adapting our data structures to ones GTK+ can understand. GTK+
	//uses a lot of these to provide the flexibilty it has
	Gtk::TreeModelColumnRecord tmcr_artist;
	tmcr_artist.add(artist_name);
	artist_listing = Gtk::ListStore::create(tmcr_artist);
	Gtk::Label artist_label("Artist");
	//And now that we have the data set up, attach it to a new combobox
	Gtk::TreeView artist_view(artist_listing);
	artist_view.append_column("Artist Name", artist_name);
	artist_selection = artist_view.get_selection();
	artist_selection->signal_changed().connect(sigc::ptr_fun(&select_artist));
	
	
	//Similar procedure for the album selector
	Gtk::TreeModelColumnRecord tmcr_album;
	tmcr_album.add(album_name);
	tmcr_album.add(album_genre);
	tmcr_album.add(album_year);
	album_listing = Gtk::ListStore::create(tmcr_album);
	Gtk::Label album_label("Album");
	Gtk::TreeView album_view(album_listing);
	album_view.append_column("Album Name", album_name);
	album_view.append_column("Genre", album_genre);
	album_view.append_column_numeric("Year", album_year, "%d");
	album_selection = album_view.get_selection();
	album_selection->signal_changed().connect(sigc::ptr_fun(&select_album));
	
	//Adding these to a toolbar eventually
	Gtk::VBox topcont(false, 0);
	toplevel->add(topcont);
	
	Gtk::HBox editbuttons(false, 0);
	topcont.pack_start(editbuttons, false, false, 4);
	
	edit_artist_button = new Gtk::Button("Edit Artist");
	edit_album_button = new Gtk::Button("Edit Album");
	edit_track_button = new Gtk::Button("Edit Track");
	edit_artist_button->signal_clicked().connect(sigc::ptr_fun(&edit_artist));
	edit_album_button->signal_clicked().connect(sigc::ptr_fun(&edit_album));
	edit_track_button->signal_clicked().connect(sigc::ptr_fun(&edit_track));
	edit_artist_button->set_sensitive(false);
	edit_album_button->set_sensitive(false);
	edit_track_button->set_sensitive(false);
	add_album_button->set_sensitive(false);
	add_track_button->set_sensitive(false);
	editbuttons.pack_start(*add_artist_button, false, false, 3);
	editbuttons.pack_start(*add_album_button, false, false, 3);
	editbuttons.pack_start(*add_track_button, false, false, 3);
	editbuttons.pack_end(*edit_track_button, false, false, 3);
	editbuttons.pack_end(*edit_album_button, false, false, 3);
	editbuttons.pack_end(*edit_artist_button, false,false, 3);
	
	Gtk::HBox deletebuttons(false, 0);
	editbuttons.set_center_widget(deletebuttons);
	
	delete_artist_button = new Gtk::Button("-Artist");
	delete_album_button = new Gtk::Button("-Album");
	delete_track_button = new Gtk::Button("-Track");
	deletebuttons.pack_start(*delete_artist_button, false, false, 3);
	deletebuttons.pack_start(*delete_album_button, false, false, 3);
	deletebuttons.pack_start(*delete_track_button, false, false, 3);
	
	
	Gtk::HBox artalbcont(false, 0);
	topcont.pack_start(artalbcont, true, true, 0);
	artalbcont.pack_start(artist_view, true, true, 2);
	artalbcont.pack_start(album_view, true, true, 2);
	
	//Now for the GTK+ specific data structures and complications to make
	//tables show up correctly.
	Gtk::TreeModelColumnRecord tmcr;
	
	tmcr.add(track_name);
	tmcr.add(track_time);
	track_listing = Gtk::ListStore::create(tmcr);
	Gtk::TreeView track_view(track_listing);
	track_selection = track_view.get_selection();
	topcont.pack_start(track_view, true, true, 4);
	track_view.append_column("Track Name", track_name);
	track_view.append_column("Track Time", track_time);
	track_selection->signal_changed().connect(sigc::ptr_fun(&select_track));
	
	toplevel->show_all();
	return app->run(*toplevel);
}
