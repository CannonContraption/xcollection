# XCollection
## Author
(C) Jan-Feb. '17 James Read
Date of completion: 2/15/17

## Brief Description
XCollection is a music collection manager. It contains basic command-line syntax for adding records to either the artist list (collection), album list (tied to each artis), or track list (tied to each album). It also allows loading from and saving to files.
