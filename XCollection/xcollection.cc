#include "xcollection.hh"

/*
 * Windows doesn't support color escape codes
 * but they're wicked handy elsewhere so use
 * macros to test platform and use them if it's
 * not Windows
 */
#if !defined _WIN32
string reset = "\033[m";
string boldm = "\033[1;35m";
string regum = "\033[35m";
string rvidy = "\033[7;33m";
#endif
#if defined _WIN32
string reset = "";
string boldm = "";
string regum = "";
string rvidy = "";
#endif


artist xcollection::addartist(string name){
	artist newartist;
	newartist.name = name;
	collection.push_back(newartist);
	return newartist;
}

album xcollection::addalbum(string artistname, string albumname, string genre, int year){
	bool foundartist = false;
	artist * subject;
	for(int i = 0; i<collection.size(); i++){
		if(collection.getelement(i)->name == artistname){
			foundartist = true;
			subject = collection.getelement(i);
			break;
		}
	}
	if(!foundartist){
		artist temp = addartist(artistname);
		collection.push_back(temp);
		subject = collection.getelement(collection.size()-1);
	}
	album thisalbum;
	thisalbum.name=albumname;
	thisalbum.genre=genre;
	thisalbum.year=year;
	subject->albums.push_back(thisalbum);
	return thisalbum;
}

track xcollection::addtrack(string artistname, string albumname, string trackname, int hours,  int minutes, int seconds){
	bool foundalbum = false;
	bool foundartist = false;
	album * selectedalbum;
	for(int i = 0; i<collection.size(); i++){
		if(collection.getelement(i)->name == artistname){
			foundartist = true;
			for(int j = 0; j<collection.getelement(i)->albums.size(); j++){
				if(collection.getelement(i)->albums.getelement(j)->name == albumname){
					foundalbum = true;
					selectedalbum = collection.getelement(i)->albums.getelement(j);
				}
			}
		}
	}
	if(!foundartist) addartist(artistname);
	if(!foundalbum) addalbum(artistname, albumname, "unknown",0);
	track newtrack;
	newtrack.title=trackname;
	newtrack.hours=hours;
	newtrack.minutes=minutes;
	newtrack.seconds=seconds;
	selectedalbum->tracks.push_back(newtrack);
	return newtrack;
}

bool xcollection::showtracks(string artistn, string albumn){
	artist * selectedartist;
	album * selectedalbum;
	bool found = false;
	for(int i = 0; i<collection.size(); i++){
		if(collection.getelement(i)->name == artistn){
			selectedartist = collection.getelement(i);
			found = true;
		}
	}
	if(!found){
		cerr<<"Couldn't find artist."<<endl;
		return false;
	}
	found = false;
	for(int i = 0;i<selectedartist->albums.size(); i++){
		if(selectedartist->albums.getelement(i)->name == albumn){
			selectedalbum = selectedartist->albums.getelement(i);
		}
	}
	if(!found){
		cerr<<"Couldn't find album."<<endl;
		return false;
	}
	cout<<"Track List -------------------------------------------"<<endl;
	for(int i = 0; i<selectedalbum->tracks.size(); i++){
		cout<<"                    "<<selectedalbum->tracks.getelement(i)->title<<"\r";
		cout<<selectedalbum->tracks.getelement(i)->hours<<':'<<selectedalbum->tracks.getelement(i)->minutes<<':'<<selectedalbum->tracks.getelement(i)->seconds<<endl;
	}
	return true;
}

bool xcollection::showtracks_fromelement(album * selectedalbum){
	cout<<"Track List -------------------------------------------"<<endl;
	for(int i = 0; i<selectedalbum->tracks.size(); i++){
		cout<<"                    "<<selectedalbum->tracks.getelement(i)->title<<"\r";
		cout<<setfill(' ')<<setw(2)<<selectedalbum->tracks.getelement(i)->hours<<':'<<setfill('0')<<setw(2)<<selectedalbum->tracks.getelement(i)->minutes<<':'<<setfill('0')<<setw(2)<<selectedalbum->tracks.getelement(i)->seconds<<endl;
	}
	return true;
}

bool xcollection::showalbums(string artistn, bool tracks){
	artist * selectedartist;
	bool found = false;
	for(int i = 0; i<collection.size(); i++){
		if(collection.getelement(i)->name == artistn){
			selectedartist = collection.getelement(i);
			found = true;
		}
	}
	if(!found){
		cerr<<"Couldn't find artist."<<endl;
		return false;
	}
	cout<<"Album List ==========================================="<<endl;
	int albumcount = selectedartist->albums.size();
	for(int i = 0; i<albumcount; i++){
		cout<<"                    "<<selectedartist->albums.getelement(i)->name<<"\r";
		cout<<selectedartist->albums.getelement(i)->genre<<endl;
		bool worked;
		if (tracks)
			worked = showtracks(artistn, selectedartist->albums.getelement(i)->name);
		if(worked==false)
			cout<<"No tracks found."<<endl;
	}
}

bool xcollection::showalbums_fromelement(artist * selectedartist, bool tracks){
	cout<<"Album List ==========================================="<<endl;
	int albumcount = selectedartist->albums.size();
	for(int i = 0; i<albumcount; i++){
		cout<<endl;
		cout<<"                         "<<regum<<selectedartist->albums.getelement(i)->name<<reset<<"\r";
		cout<<"     "<<selectedartist->albums.getelement(i)->genre;
		cout<<"\r"<<boldm<<selectedartist->albums.getelement(i)->year<<reset<<endl;
		bool worked;
		if (tracks)
			worked = showtracks_fromelement((selectedartist->albums.getelement(i)));
		else
			worked = true;
		if(worked==false)
			cout<<"No tracks found."<<endl;
	}
}

bool xcollection::showartists(bool albums, bool tracks){
	for(int i = 0; i<collection.size(); i++){
		cout<<endl<<rvidy<<"######################################################"<<reset<<"\r";
		cout<<rvidy<<collection.getelement(i)->name<<" "<<reset<<endl;
		if(albums)
			showalbums_fromelement((collection.getelement(i)), tracks);
	}
}

bool xcollection::editartist(string oldname, string newname){
	bool got = false;
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == oldname)
			collection[i].name = newname;
			got = true;
			break;
	}
	return got;
}

bool xcollection::editalbum(string artist, string oldname, string newname, string newgenre, int newyear){
	int albumindex = -1;
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == artist){
			albumindex = i;
			break;
		}
	}
	if(albumindex == -1) return false;
	for(int i = 0; i<collection[albumindex].albums.size(); i++){
		if(collection[albumindex].albums[i].name == oldname){
			collection[albumindex].albums[i].name = newname;
			collection[albumindex].albums[i].genre = newgenre;
			collection[albumindex].albums[i].year = newyear;
		}
	}
	return true;
}

bool xcollection::edittrack(string artist, string album, string oldname, string newname, int newh, int newm, int news){
	int albumindex = -1;
	int trackindex = -1;
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == artist){
			albumindex = i;
			break;
		}
	}
	if(albumindex == -1) return false;
	for(int i = 0; i<collection[albumindex].albums.size(); i++){
		if(collection[albumindex].albums[i].name == album){
			trackindex = i;
			break;
		}
	}
	if(trackindex == -1) return false;
	for(int i = 0; i<collection[albumindex].albums[trackindex].tracks.size(); i++){
		if(collection[albumindex].albums[trackindex].tracks[i].title == oldname){
			collection[albumindex].albums[trackindex].tracks[i].title = newname;
			collection[albumindex].albums[trackindex].tracks[i].hours = newh;
			collection[albumindex].albums[trackindex].tracks[i].minutes = newm;
			collection[albumindex].albums[trackindex].tracks[i].seconds = news;
			trackindex = -1;
		}
	}
	if(trackindex != -1) return false;
}

bool xcollection::deleteartist(string artist){
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == artist){
			for(int j = 0; j<collection[i].albums.size(); j++){
				collection[i].albums[j].tracks.clearall();
			}
			collection[i].albums.clearall();
			collection.clearone(i);
		}
	}
}

bool xcollection::deletealbum(string artist, string album){
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == artist){
			for(int j = 0; j<collection[i].albums.size(); j++){
				if(collection[i].albums[j].name == album){
					collection[i].albums[j].tracks.clearall();
					collection[i].albums.clearone(j);
				}
			}
		}
	}
}

bool xcollection::deletetrack(string artist, string album, string track){
	for(int i = 0; i<collection.size(); i++){
		if(collection[i].name == artist){
			for(int j = 0; j<collection[i].albums.size(); j++){
				if(collection[i].albums[j].name == album){
					for(int k = 0; k<collection[i].albums[j].tracks.size(); k++){
						if(collection[i].albums[j].tracks[k].title == track){
							collection[i].albums[j].tracks.clearone(k);
						}
					}
				}
			}
		}
	}
}

int xcollection::clearcollection(){
	int numempty = 0;
	for(int i = 0; i<collection.size(); i++){
		for(int j = 0; j<collection[i].albums.size(); j++){
			if(!collection[i].albums[j].tracks.clearall()) numempty++;
		}
		if(!collection[i].albums.clearall()) numempty++;
	}
	if(!collection.clearall()) numempty++;
}
