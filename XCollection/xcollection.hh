#include<iostream>
#include<iomanip>

using namespace std;

/*
 * These structs are there to store individual data points. Defining these
 * as structs means that there can be lists for each of these which will
 * handle storing each track's information together in one place instead of
 * across whole classes (which is overkill) or across multiple lists for
 * each data field (which is error-prone and messy)
 */

struct track{
	string title;
	int hours;
	int minutes;
	int seconds;
};

struct album{
	string name;
	string genre;
	int year;
	singlelinked<track> tracks;
};

struct artist{
	string name;
	singlelinked<album> albums;
};

/*
 * this is a class so that multiple concurrent collections can be used and
 * enabled/disabled on demand.
 */
class xcollection{
public:
	singlelinked<artist> collection;
	artist addartist(string name);
	album addalbum(string artistname, string albumname, string genre, int year);
	track addtrack(string artistname, string albumname, string trackname, int hours, int minutes, int seconds);
	bool showalbums(string artistn, bool tracks);
	bool showartists(bool albums, bool tracks);
	bool showtracks(string artistn, string albumn);
	bool showalbums_fromelement(artist * selectedartist, bool tracks);
	bool showtracks_fromelement(album * selectedalbum);
	bool editartist(string oldname, string newname);
	bool editalbum(string artist, string oldname, string newname, string newgenre, int newyear);
	bool edittrack(string artist, string album, string oldname, string newname, int newh, int newm, int news);
	bool deleteartist(string artist);
	bool deletealbum(string artist, string album);
	bool deletetrack(string artist, string album, string track);
	int clearcollection(); //returns num empty
};
