#include<fstream>
int parsefile(xcollection * collection, string filename){
	collection->clearcollection();
	enum level{
		artistlevel,
		albumlevel,
		tracklevel
	};
	bool works = false;
	level currentlevel;
	currentlevel = artistlevel;
	ifstream filetoread;
	filetoread.open(filename);
	if(!filetoread.is_open()) return 2;
	string currentline;
	string key;
	string value;
	string header;
	int spaces = 0;
	string artistn;
	string albumn;
	string values[4]; //common values label.
	while(!filetoread.fail()){
		getline(filetoread, currentline);
		spaces = 0;
		key = "";
		value = "";
		//cout<<currentline<<endl;
		for(char c : currentline){
			if(c == '\n') spaces = 0;
			if(c == ' '){
				if(spaces > 1) value += ' ';
				spaces++;
				continue;
			}
			if(spaces == 0){
				key += c;
			}
			else if(spaces > 1){
				value += c;
			}
		}
		//cout<<key<<"  "<<value<<endl;
		if(key[0] == '['){
			//cout<<key<<endl;
			if(currentlevel == tracklevel){
				collection->addtrack(artistn, albumn, values[0], stoi(values[1]), stoi(values[2]), stoi(values[3]));
			}
			if(currentlevel == albumlevel){
				collection->addalbum(artistn, values[0], values[1], stoi(values[2]));
				albumn = values[0];
			}
			if(key == "[artist]") currentlevel = artistlevel;
			else if (key == "[album]"){
				currentlevel = albumlevel;
				works = true;
			}
			else if (key == "[track]"){
				currentlevel = tracklevel;
			}
		}
		else{
			if(currentlevel == artistlevel){
				//cout<<"artist"<<endl;
				if(key == "name") artistn = value;
				collection -> addartist(value);
			}
			if(currentlevel == albumlevel){
				//cout<<"album"<<endl;
				if(key == "title")
					values[0] = value;
				else if (key == "genre")
					values[1] = value;
				else if (key == "year")
					values[2] = value;
			}
			if(currentlevel == tracklevel){
				//cout<<"track"<<endl;
				if(key == "name")
					values[0] = value;
				if(key == "hours")
					values[1] = value;
				if(key == "minutes")
					values[2] = value;
				if(key == "seconds")
					values[3] = value;
			}
		}
	}
	if(currentlevel == tracklevel){
		collection->addtrack(artistn, albumn, values[0], stoi(values[1]), stoi(values[2]), stoi(values[3]));
	}
	if(currentlevel == albumlevel){
		collection->addalbum(artistn, values[0], values[1], stoi(values[2]));
		albumn = values[0];
	}
	filetoread.close();
	if(works) return 0;
	return 1;
}
