int savefile(xcollection * collection, string fname){
	ofstream sf;
	fname+=".ini";
	sf.open(fname, ios::out);
	if(!sf.is_open()) return 2;
	for(int i = 0; i<collection->collection.size(); i++){
		artist * currentartist = collection->collection.getelement(i);
		sf<<"[artist]"<<endl;
		sf<<"name = "<<currentartist->name<<endl;
		int albumcount = currentartist->albums.size();
		for(int j = 0; j<albumcount; j++){
			album * currentalbum = currentartist->albums.getelement(j);
			sf<<"[album]"<<endl;
			sf<<"title = "<<currentalbum->name<<endl;
			sf<<"genre = "<<currentalbum->genre<<endl;
			sf<<"year = "<<currentalbum->year<<endl;
			for(int i = 0; i<currentalbum->tracks.size(); i++){
				sf<<"[track]"<<endl;
				sf<<"name = "<<currentalbum->tracks.getelement(i)->title<<endl;
				sf<<"hours = "<<currentalbum->tracks.getelement(i)->hours<<endl;
				sf<<"minutes = "<<currentalbum->tracks.getelement(i)->minutes<<endl;
				sf<<"seconds = "<<currentalbum->tracks.getelement(i)->seconds<<endl;
			}
		}
	}
	sf.close();
	return 0;
}
